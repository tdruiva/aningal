+++
audios = []
descricao = "A PAISAGEM AMAZÔNICA, ENTRE PELE, CORPO E AMBIENTE \n\nCláudia Leão"
galeria = []
tags = ["LABampe", "Artigo Científico"]
title = "A PAISAGEM AMAZÔNICA, ENTRE PELE, CORPO E AMBIENTE"
[capa]
descricao = ""
imagem = ""
[[links_relacionados]]
link = "https://"
titulo = ""
[[pdfs]]
ano = 2016
arquivo = "imagens/cl_a-paisagem-amazonica-entre-pele-coorpo-e-ambiente_-final2.pdf"
autoria = "Cláudia Leão"
titulo = "A PAISAGEM AMAZÔNICA, ENTRE PELE, CORPO E AMBIENTE"

+++
