+++
audios = []
descricao = "### Estalos, incidentes e acontecimentos \n\n#### como procedimento e método da pesquisa em artes \n\n## Proêmio \n\n**Cláudia Leão Maria dos Remédios de Brito** \n\nEstalos: retumba, quebra, fissura, fendir para abrir espaço, lugares... Estalos também podem ser entendidos como alguma coisa que atravessa o corpo, solicita uma variação, deslocamento... Estalos são alguma coisa que acorda o corpo para encontros. As travessias são colocadas nessas páginas, elas percorrem o campo das composições singulares, apostas, linhas intercruzadas que tendem a saltar da mera materialidade dos fazeres em artes. Processos Metodológicos na Pesquisa em Artes, muito mais do que um percurso linear que envolva a boa vontade do pesquisador; ancora um diálogo com o aprendizado na composição de blocos sensíveis, pensando o fazer em artes como uma transmutação da matéria a ser pesquisada, saltando as objetividades, as meras subjetividades para percorrer a qualidade de verdadeiras invenções de mundos. Então, a pesquisa em Artes passa por um aprendizado, um corpo a corpo despojado da lógica representacional para entrar em um campo diferencial no seu criar. O livro Estalos, Incidentes, Acontecimentos como procedimentos e Métodos da Pesquisa em Artes não objetiva trazer modelos para a pesquisa no campo das artes, mas com certeza quer firmar espaços para a construção de pesquisa em artes produzida na/da região norte, região das Amazônias e suas complexidades, trazendo assim experiências que advêm com um trabalho diário, intenso com o modo fazer artístico, estudos, corpos singulares, experimentações por seus desejos e ações inscritas em corpos políticos, por seus embalos, acontecimentos, estalos em blocos sensíveis, sinalizando que o pensamento, modos de fazer em Artes, pode e deve vir antes. Não queremos o lugar do cientificismo, queremos o nosso lugar em todas as suas possibilidades em/na/das Artes... As organizadoras e as colaboradoras e os colaboradores que compõem cada linha, cada parte, pequena parte desse livro-encontro-livro-afeto-livro-vida-livro\u0002arte desejam que ele seja respiros, frestas, aberturas, fendas, bonitezas para a criação de sentidos outros no campo da pesquisa em Artes. Que a leitura seja provocadora, libertadora, que ela seja para invenções de mundo possíveis, pois é justo ter direito à criação.\n\nconcepção **Cláudia Leão e Maria dos Remédios de Brito** \n\nedição de textos **Dhemersson Warly Santos Costa e Maria dos Remédios de Brito** \n\nprojeto gráfico e diagramação **José Viana**\n\n direção de arte **Cláudia Leão e José Viana** \n\nfotografias **Cláudia Leão** (2017, 2018 e 2019) **Alexis Matute, Ramon Reis e Wilka Sales (2019)** \n\nrevisão textual e bibliográfica **Os/as próprios/próprias autores/autoras dos textos, artigos, cartas e poesias. ficha catalográfica Larissa Silva**"
galeria = []
links_relacionados = []
tags = ["LABampe"]
title = "Estalos, incidentes e acontecimentos como  procedimento e método da pesquisa em arte"
[capa]
descricao = ""
imagem = ""
[[pdfs]]
ano = 2020
arquivo = ""
autoria = "ORG. Cláudia Leão e Maria dos Remédios de Brito"
titulo = "livro_Estalos Incidentes e Acontecimentos_Compactado"

+++
